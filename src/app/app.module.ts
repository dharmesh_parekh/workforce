import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { DashboardComponent } from './dashboard/dashboard.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { AppheaderComponent } from './_layout/appheader/appheader.component';
import { AppmenuComponent } from './_layout/appmenu/appmenu.component';
import { AppsettingsComponent } from './_layout/appsettings/appsettings.component';
import { AppfooterComponent } from './_layout/appfooter/appfooter.component';
import { AppLayoutComponent } from './_layout/applayout/applayout.component';

@NgModule({
  declarations: [
    AppComponent,
    AppheaderComponent,
    AppmenuComponent,
    AppsettingsComponent,
    AppfooterComponent,
    DashboardComponent,
    AppLayoutComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    RouterModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
