import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appmenu',
  templateUrl: './appmenu.component.html',
  styleUrls: ['./appmenu.component.css']
})
export class AppmenuComponent implements OnInit {

  userInfo: any;

  constructor(private router: Router) { }


  ngOnInit() {
    this.userInfo = JSON.parse(localStorage.getItem("LoggedInUserInfo"));
  }

}
