import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-appheader',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css']
})
export class AppheaderComponent implements OnInit {

  userInfo: any;

  constructor(private router: Router) { }


  ngOnInit() {   
    this.userInfo = JSON.parse(localStorage.getItem("LoggedInUserInfo"));
  }

  logout() {
    localStorage.removeItem('userToken');
    this.router.navigate(['/login']);
  }

}
