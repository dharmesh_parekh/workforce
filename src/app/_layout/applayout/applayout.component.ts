import { Component, OnInit, Renderer2 } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-applayout',
  templateUrl: './applayout.component.html',
  styleUrls: ['./applayout.component.css']
})
export class AppLayoutComponent implements OnInit {

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
    this.renderer.removeClass(document.body, 'login-page');
    this.renderer.addClass(document.body, 'skin-site');
    this.renderer.addClass(document.body, 'sidebar-mini');
    $('body').layout('fix');
  }
}
